<?php
namespace Tests;

use PHPUnit\Framework\TestCase;

class LoginTest extends TestCase{
    public function testPotencia(){
        $l = new \Rapi100\Controller\Login(null);
        
        $this->assertEquals(4, 
        $l->potencia(2, 2));

        $this->assertEquals(8, 
        $l->potencia(2, 3));

        $this->assertEquals(-8, 
        $l->potencia(-2, 3));
    }
}