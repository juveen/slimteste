function Pessoa(nome, sobrenome, idade, genero, interesses) {
    this.nome = {
        nome: nome,
        sobrenome: sobrenome
    };
    this.idade = idade;
    this.genero = genero;
    this.interesses = interesses;

    this.biografia = function(){
        console.log(this.nome.nome + ' ' +
            this.nome.sobrenome + ' tem ' + this.idade + ' anos e gosta de ' +
            this.interesses
        );
    };

    this.apresentar = function(){
        console.log('Olá ' + this.nome.nome);
        console.log(this);
    }
}

var maria = new Pessoa('Maria', 'Silva', 18, 'Feminino', 'Música');

Pessoa.prototype.sleep = function(){
    console.log(this.nome.nome + ' está dormindo');
}

console.log(maria.nome.nome);
maria.biografia();
maria.apresentar();
maria.sleep();
console.log(Pessoa.prototype, Object.prototype);
