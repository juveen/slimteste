<?php
namespace Rapi100\Controller;

class Login{
    private $container = null;
    public function __construct($container){
        $this->container = $container;
    }
    public function logar(\Slim\Http\Request $request, \Slim\Http\Response $response, array $args) {
        // Sample log message
        $this->container->logger->info("Slim-Skeleton '/login' route");
    
        // Render index view
        return $this->container->renderer->
        render($response, 'login.html', $args);
    }

    public function potencia($base, 
    $expoente){
        $resultado = pow($base, $expoente);
        return $resultado;
    }
}