<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/login', '\Rapi100\Controller\Login:logar');

$app->post('/enviarredacao', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $response->getBody()
    ->write('Esssa é a página de enviar redação');
});

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
